﻿
// Sources: https://github.com/willemt/array-avl-tree.git and http://www.superstarcoders.com/blogs/posts/efficient-avl-tree-in-c-sharp.aspx

using UnityEngine;
using SearchNode = AStarPathFinding.SearchNode;

public class ArrayAVLTree {

    const int NULL = -1;

    public struct node {
        public float key;
        public SearchNode value;
        public short balance;

        public node(float key, SearchNode value) {
            this.key = key;
            this.value = value;
            this.balance = 0;
        }

        override public string ToString() {
            return "Key: " + key + " Bal: " + balance + " Value: " + value;
        }
    }

    int size;
    int count;
    node[] nodes;


    public int Size
    {
        get { return size; }
    }

    public int Count
    {
        get { return count; }
    }

    public int Height
    {
        get { return getHeight(0); }
    }

    public node nodeAt(int indx) {
        return nodes[indx];
    }

    public ArrayAVLTree(int initalSize) {
        nodes = new node[initalSize];
        size = nodes.Length;
        count = 0;

        for (int i = 0; i < size; i++) {
            nodes[i].key = NULL;
        }
    }

    static int max(int x, int y) {
        return ((x < y) ? y : x);
    }

    static int lChild(int indx) {
        return indx * 2 + 1;
    }

    static int rChild(int indx) {
        return indx * 2 + 2;
    }

    static int parent(int indx) {
        if (indx == 0) {
            return NULL;
        } else {
            return (indx - 1) / 2;
        }
    }

    int getCount(int indx) {
        if (indx >= size || nodes[indx].key == NULL) {
            return 0;
        } else {
            return getCount(lChild(indx)) + getCount(rChild(indx)) + 1;
        }
    }

    int getHeight(int indx) {
        if (indx >= size || nodes[indx].key == NULL) {
            return 0;
        } else {
            return max(getHeight(lChild(indx)) + 1, getHeight(rChild(indx)) + 1);
        }
    }

    void shiftUp(int indx, int towards) {
        if (indx >= size || nodes[indx].key == NULL) {
            return;
        }
        nodes[towards] = nodes[indx];
        nodes[indx].key = NULL;
        shiftUp(lChild(indx), lChild(towards));
        shiftUp(rChild(indx), rChild(towards));
    }

    void shiftDown(int indx, int towards) {
        if (indx >= size || nodes[indx].key == NULL) {
            return;
        }

        // increase size so we can finish shifting down
        while (towards >= size) { // while in the case we don't make it big enough
            enlarge();
        }

        shiftDown(lChild(indx), lChild(towards));
        shiftDown(rChild(indx), rChild(towards));
        nodes[towards] = nodes[indx];
        nodes[indx].key = NULL;
    }

    void rotateRight(int rootIndx) {
        int pivotIndx = lChild(rootIndx);

        // shift the roots right subtree down to the right
        shiftDown(rChild(rootIndx), rChild(rChild(rootIndx)));
        nodes[rChild(rootIndx)] = nodes[rootIndx]; // move root too

        // move the pivots right child to the roots right child's left child
        shiftDown(rChild(pivotIndx), lChild(rChild(rootIndx)));

        // move the pivot up to the root
        shiftUp(pivotIndx, rootIndx);

        // adjust balances of nodes in their new positions
        nodes[rootIndx].balance--; // old pivot
        nodes[rChild(rootIndx)].balance = (short)(-nodes[rootIndx].balance); // old root
    }

    void rotateLeft(int rootIndx) {
        int pivotIndx = rChild(rootIndx);

        // Shift the roots left subtree down to the left
        shiftDown(lChild(rootIndx), lChild(lChild(rootIndx)));
        nodes[lChild(rootIndx)] = nodes[rootIndx]; // move root too

        // move the pivots left child to the roots left child's right child
        shiftDown(lChild(pivotIndx), rChild(lChild(rootIndx)));

        // move the pivot up to the root
        shiftUp(pivotIndx, rootIndx);

        // adjust balances of nodes in their new positions
        nodes[rootIndx].balance++; // old pivot
        nodes[lChild(rootIndx)].balance = (short)(-nodes[rootIndx].balance); // old root
    }


    // Where rootIndx is the highest point in the rotating tree
    // not the root of the first Left rotation
    void rotateLeftRight(int rootIndx) {
        int newRootIndx = rChild(lChild(rootIndx));

        // shift the root's right subtree down to the right
        shiftDown(rChild(rootIndx), rChild(rChild(rootIndx)));
        nodes[rChild(rootIndx)] = nodes[rootIndx];

        // move the new roots right child to the roots right child's left child
        shiftUp(rChild(newRootIndx), lChild(rChild(rootIndx)));

        // move the new root node into the root node
        nodes[rootIndx] = nodes[newRootIndx];
        nodes[newRootIndx].key = NULL;

        // shift up to where the new root was, it's left child
        shiftUp(lChild(newRootIndx), newRootIndx);

        // adjust balances of nodes in their new positions
        if (nodes[rootIndx].balance == -1) { // new root
            nodes[rChild(rootIndx)].balance = 0; // old root
            nodes[lChild(rootIndx)].balance = 1; // left from old root
        } else if (nodes[rootIndx].balance == 0) {
            nodes[rChild(rootIndx)].balance = 0;
            nodes[lChild(rootIndx)].balance = 0;
        } else {
            nodes[rChild(rootIndx)].balance = -1;
            nodes[lChild(rootIndx)].balance = 0;
        }

        nodes[rootIndx].balance = 0;
    }

    // Where rootIndx is the highest point in the rotating tree
    // not the root of the first Left rotation
    void rotateRightLeft(int rootIndx) {
        int newRootIndx = lChild(rChild(rootIndx));

        // shift the root's left subtree down to the left
        shiftDown(lChild(rootIndx), lChild(lChild(rootIndx)));
        nodes[lChild(rootIndx)] = nodes[rootIndx];

        // move the new roots left child to the roots left child's right child
        shiftUp(lChild(newRootIndx), rChild(lChild(rootIndx)));

        // move the new root node into the root node
        nodes[rootIndx] = nodes[newRootIndx];
        nodes[newRootIndx].key = NULL;

        // shift up to where the new root was it's right child
        shiftUp(rChild(newRootIndx), newRootIndx);

        // adjust balances of nodes in their new positions
        if (nodes[rootIndx].balance == 1) { // new root
            nodes[lChild(rootIndx)].balance = 0; // old root
            nodes[rChild(rootIndx)].balance = -1; // right from old root
        } else if (nodes[rootIndx].balance == 0) {
            nodes[lChild(rootIndx)].balance = 0;
            nodes[rChild(rootIndx)].balance = 0;
        } else {
            nodes[lChild(rootIndx)].balance = 1;
            nodes[rChild(rootIndx)].balance = 0;
        }

        nodes[rootIndx].balance = 0;
    }

    void insertBalance(int pviotIndx, short balance) {
        while (pviotIndx != NULL) {
            balance = (nodes[pviotIndx].balance += balance);

            if (balance == 0) {
                return;
            } else if (balance == 2) {
                if (nodes[lChild(pviotIndx)].balance == 1) {
                    rotateRight(pviotIndx);
                } else {
                    rotateLeftRight(pviotIndx);
                }
                return;
            } else if (balance == -2) {
                if (nodes[rChild(pviotIndx)].balance == -1) {
                    rotateLeft(pviotIndx);
                } else {
                    rotateRightLeft(pviotIndx);
                }
                return;
            }

            int p = parent(pviotIndx);

            if (p != NULL) {
                balance = lChild(p) == pviotIndx ? (short)1 : (short)-1;
            }

            pviotIndx = p;
        }
    }

    void deleteBalance(int pviotIndx, short balance) {
        while (pviotIndx != NULL) {
            balance = (nodes[pviotIndx].balance += balance);

            if (balance == 2) {
                if (nodes[lChild(pviotIndx)].balance >= 0) {
                    rotateRight(pviotIndx);

                    if (nodes[pviotIndx].balance == -1) {
                        return;
                    }
                } else {
                    rotateLeftRight(pviotIndx);
                }
            } else if (balance == -2) {
                if (nodes[rChild(pviotIndx)].balance <= 0) {
                    rotateLeft(pviotIndx);

                    if (nodes[pviotIndx].balance == 1) {
                        return;
                    }
                } else {
                    rotateRightLeft(pviotIndx);
                }
            } else if (balance != 0) {
                return;
            }

            int p = parent(pviotIndx);

            if (p != NULL) {
                balance = lChild(p) == pviotIndx ? (short)-1 : (short)1;
            }

            pviotIndx = p;
        }
    }

    public bool search(float key, out SearchNode sNode) {
        for (int i = 0; i < size;) {

            /* couldn't find it */
            if (nodes[i].key == NULL) {
                sNode = default(SearchNode);
                return false;
            }

            if (key == nodes[i].key) {
                sNode = nodes[i].value;
                return true;
            } else if (key < nodes[i].key) {
                i = lChild(i);
            } else if (key > nodes[i].key) {
                i = rChild(i);
            }
        }

        /* couldn't find it */
        sNode = default(SearchNode);
        return false;
    }

    public bool search(float key) {
        for (int i = 0; i < size;) {

            /* couldn't find it */
            if (nodes[i].key == NULL) {
                return false;
            }

            if (key == nodes[i].key) {
                return true;
            } else if (key < nodes[i].key) {
                i = lChild(i);
            } else if (key > nodes[i].key) {
                i = rChild(i);
            }
        }

        /* couldn't find it */
        return false;
    }

    // removes and returns value from tree given by key
    bool getValue(float key, out SearchNode sNode) {
        short balance = 0;
        for (int i = 0; i < size;) {

            if (nodes[i].key == NULL) {
                sNode = default(SearchNode);
                return false;
            }

            if (key == nodes[i].key) {
                sNode = nodes[i].value;
                deleteNode(i, balance);
                return true;

            } else if (key < nodes[i].key) {
                i = lChild(i);
                balance = -1;
            } else if (key > nodes[i].key) {
                i = rChild(i);
                balance = 1;
            }
        }

        sNode = default(SearchNode);
        return false;
    }

    // requires non null node index and a balance factor baised off whitch child of it's parent it is or 0
    private void deleteNode(int i, short balance) {
        int lChildIndx = lChild(i);
        int rChildIndx = rChild(i);

        count--;
        if (nodes[lChildIndx].key == NULL) {
            if (nodes[rChildIndx].key == NULL) {

                // root or leaf
                nodes[i].key = NULL;
                if (i != 0) {
                    deleteBalance(parent(i), balance);
                }
            } else {
                shiftUp(rChildIndx, i);
                deleteBalance(i, 0);
            }
        } else if (nodes[rChildIndx].key == NULL) {
            shiftUp(lChildIndx, i);
            deleteBalance(i, 0);
        } else {
            int successorIndx = rChildIndx;

            // replace node with smallest child in the right subtree
            if (nodes[lChild(successorIndx)].key == NULL) {
                nodes[successorIndx].balance = nodes[i].balance;
                shiftUp(successorIndx, i);
                deleteBalance(successorIndx, 1);
            } else {
                int tempLeft;
                while ((tempLeft = lChild(successorIndx)) != NULL) {
                    successorIndx = tempLeft;
                }
                nodes[successorIndx].balance = nodes[i].balance;
                nodes[i] = nodes[successorIndx];
                shiftUp(rChild(successorIndx), successorIndx);
                deleteBalance(parent(successorIndx), -1);
            }
        }
    }

    // removes and returns the minimum key
    public bool getMin(out SearchNode sNode) {
        int i = 0;

        // get the left most node/child
        for (int next = lChild(i); next < size && nodes[next].key != NULL; i = next, next = lChild(i));

        if (nodes[i].key == NULL) {
            sNode = default(SearchNode);
            return false;
        }

        sNode = nodes[i].value;
        if (i == 0) {
            deleteNode(i, 0);
        } else {
            deleteNode(i, -1);
        }

        return true;
    }

    // removes and returns the maximum key
    public bool getMax(out SearchNode sNode) {

        int i = 0;

        // get the right most node/child
        for (int next = rChild(i); next < size && nodes[next].key != NULL; i = next, next = rChild(i))
            ;

        if (nodes[i].key == NULL) {
            sNode = default(SearchNode);
            return false;
        }

        sNode = nodes[i].value;
        if (i == 0) {
            deleteNode(i, 0);
        } else {
            deleteNode(i, 1);
        }

        return true;
    }

    public void clear() {
        for (int i = 0; i < size; i++) {
            nodes[i].key = NULL;
        }
    }

    public bool insert(float key, SearchNode value) {
        int i;
        short balance = 0;

        for (i = 0; i < size;) {

            // insert into empty slot
            if (nodes[i].key == NULL) {
                nodes[i].key = key;
                nodes[i].value = value;
                count++;

                if (i == 0) {
                    return true;
                } else {
                    insertBalance(parent(i), balance);
                    return true;
                }
            }

            // keep looking for empty slot or fail if key is not unique
            if (key == nodes[i].key) {
                return false;
            } else if (key < nodes[i].key) {
                i = lChild(i);
                balance = 1;
            } else if (key > nodes[i].key) {
                i = rChild(i);
                balance = -1;
            }
        }

        // increase size
        enlarge();
        nodes[i].key = key;
        nodes[i].value = value;
        count++;
        insertBalance(parent(i), balance);
        return true;
    }

    void enlarge() {

        /* double capacity */
        size *= 2;
        node[] temp = new node[size];

        /* copy old data across to new array */
        for (int i = 0; i < size; i++) {
            if (i < (size / 2) && nodes[i].key != NULL) {
                temp[i] = nodes[i];
            } else {
                temp[i].key = NULL;
            }
        }

        /* swap arrays */
        nodes = temp;
    }
}
